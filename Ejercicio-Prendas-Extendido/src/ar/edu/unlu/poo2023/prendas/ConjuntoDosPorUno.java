package ar.edu.unlu.poo2023.prendas;

import ar.edu.unlu.poo2023.tarjetas.Tarjeta;

public class ConjuntoDosPorUno extends Conjunto {

    public ConjuntoDosPorUno(Remera remera, Prenda prenda){
        super(remera, prenda);
    }

    public double calcularPrecio(){
        double precioFinal = 0.0;
        double valorRemera = this.getRemera().calcularPrecio();
        double valorPrenda = this.getPrenda().calcularPrecio();
        if (valorRemera >= valorPrenda){
            precioFinal = valorRemera;
        } else {
            precioFinal = valorPrenda;
        }
        return precioFinal;
    }

    public double calcularPrecio(Tarjeta tarjeta){
        
    }
}
