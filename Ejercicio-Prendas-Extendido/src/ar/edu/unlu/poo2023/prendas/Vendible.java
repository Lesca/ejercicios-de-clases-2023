package ar.edu.unlu.poo2023.prendas;

import ar.edu.unlu.poo2023.tarjetas.Tarjeta;

public interface Vendible {

    public double calcularPrecio();
    public double calcularPrecio(Tarjeta tarjeta);
}
