package ar.edu.unlu.poo2023.prendas;

import ar.edu.unlu.poo2023.tarjetas.Tarjeta;

public abstract class Conjunto implements Vendible {
    private Remera remera;
    private Prenda prenda;

    public Conjunto(Remera remera, Prenda prenda){
        this.remera = remera;
        this.prenda = prenda;
    }

    public Remera getRemera() {
        return remera;
    }

    public Prenda getPrenda() {
        return prenda;
    }
}
