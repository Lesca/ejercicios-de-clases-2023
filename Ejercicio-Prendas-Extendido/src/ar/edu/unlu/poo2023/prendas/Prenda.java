package ar.edu.unlu.poo2023.prendas;

import ar.edu.unlu.poo2023.tarjetas.Tarjeta;

public abstract class Prenda implements Vendible {
    private double precioDeLista;

    public Prenda(double precioDeLista){
        this.setPrecioDeLista(precioDeLista);
    }

    protected void setPrecioDeLista(double precioDeLista){
        this.precioDeLista = precioDeLista;
    }

    public double getPrecioDeLista(){
        return this.precioDeLista;
    }
    public double calcularPrecio(){
     return this.getPrecioDeLista()*1.10;
    }

    public double calcularPrecio(Tarjeta tarjeta){
        return tarjeta.aplicarDescuento(this.calcularPrecio());
    }
}
