package ar.edu.unlu.poo2023.prendas;

public class Camisa extends Prenda {
    private boolean mangaLarga;

    public Camisa(double precioDeLista, boolean esMangaLarga){
        super(precioDeLista);
        this.mangaLarga = esMangaLarga;
    }

    public boolean isMangaLarga(){
        return mangaLarga;
    }

    public double calcularPrecio(){
        double precio = super.calcularPrecio();
        if (isMangaLarga()){
            precio += getPrecioDeLista() * 0.05;
        }
        return precio;
    }
}
