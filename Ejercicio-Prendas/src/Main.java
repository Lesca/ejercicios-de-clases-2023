import ar.edu.unlu.poo2023.prendas.*;
import ar.edu.unlu.poo2023.tarjetas.Tarjeta;
import ar.edu.unlu.poo2023.tarjetas.TarjetaDorada;

import java.util.ArrayList;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        List<Prenda> prendas = new ArrayList<Prenda>();

        Prenda remera = new Remera(1000);
        Prenda camisa = new Camisa(1000, true);
        Prenda sweater = new Sweater(1000);
        prendas.add(camisa);
        prendas.add(remera);
        prendas.add(sweater);
        System.out.println("Precio sin tarjeta: ");
        for (Prenda p: prendas){
            System.out.println("Precio: $"+p.calcularPrecio());
        }
        Tarjeta tarjeta = new TarjetaDorada();
        System.out.println("Precio con tarjeta: ");
        for (Prenda p: prendas){
            System.out.println("Precio: $"+p.calcularPrecio(tarjeta));
        }
    }
}