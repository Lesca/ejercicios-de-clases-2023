package ar.edu.unlu.poo2023.prendas;

public class Remera extends Prenda {
    public Remera(double precioDeLista){
        super(precioDeLista);
    }
    public double getPrecioDeLista(){
        return super.getPrecioDeLista() + 100;
    }
}
