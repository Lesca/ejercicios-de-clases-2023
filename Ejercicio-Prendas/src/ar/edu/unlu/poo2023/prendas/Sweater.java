package ar.edu.unlu.poo2023.prendas;

public class Sweater extends Prenda{

    public Sweater(double precioDeLista){
        super(precioDeLista);
    }

    public double calcularPrecio(){
        return this.getPrecioDeLista() * 1.08;
    }
}
